<!doctype html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <title>Parrainages 2022</title>
    <link rel="icon" type="image/png" href="src/medias/img/favicon.png"/>
    <!--STYLES-->
    <link rel="stylesheet" type="text/css" href="./src/styles/styles.css"/>
    <script src="src/js/jquery-2.2.4.min.js"></script>

</head>
<body>
<!-- LOADER -->
<script> $('html').css({ 'overflow': 'hidden', 'height': '100%' })</script>
<div class="loader-wrapper">
  <span class="loader"><span class="loader-inner"></span></span>
</div>
<script src="src/js/loading.js"></script>

    <!-- CONTENU -->
    <section>
        <img class="img-pipeline" src="https://gitlab.com/Adrien-/parrainages2022/badges/master/pipeline.svg" alt="pipeline status" >
        <div class="pb-1">
            <h1> Parrainages des candidats à l'élection présidentielle française de 2022  </h1>
        </div>

        <div class="div-btn-blanc">
			<a href="https://www.data.gouv.fr/fr/reuses/site-web-de-dataviz-parrainages-des-candidats-a-lelection-presidentielle-francaise-de-2022/" target="_blank"> <button class="btn-blanc">Réutilisations data.gouv.fr</button> </a> 
			<a href="https://gitlab.com/Adrien-/parrainages2022" target="_blank"> <button class="btn-blanc"> Dépôt GitLab </button> </a> 
        </div>

        <div class="pb-4">
            <div class="">
                <div class="">
                    <h3> Source des données : <a href="https://presidentielle2022.conseil-constitutionnel.fr/" target="_blank"> <span class="word-color">conseil-constitutionnel.fr </span> </a> et <a href="https://www.data.gouv.fr/fr/datasets/parrainages-des-candidats-a-lelection-presidentielle-francaise-de-2022/" target="_blank"> <span class="word-color"> data.gouv.fr</a> </span> </h3>
                </div>
                <div class="">
                    <h3> Date de la mise à jour : <span class="word-color"> <?php echo(json_decode(file_get_contents("src/data_out/data.json"), true)["date_maj"]) ?> </span> </h3>
                </div>
            </div>
        </div>

        <div class="container-with-centered-content pb-4">
            <h4>2022</h4>
            <div class="row">
                <div class="col-3">
                    <h2> <span class="word-color"> <?php echo(json_decode(file_get_contents("src/data_out/data.json"), true)["nb_parrainage_exp"]) ?> </span>  / <?php echo(json_decode(file_get_contents("src/data_out/data.json"), true)["nb_parrainage_possible"]) ?> <span class="legend"> <br/>Parrainages exprimés</span></h2>
                </div>
                <div class="col-3">
                    <h2> <span class="word-color"> <?php echo(json_decode(file_get_contents("src/data_out/data.json"), true)["nb_candidat"]) ?> </span> <span class="legend"> <br/>Candidats</span> </h2>
                </div>
                <div class="col-3">
                    <h2> <span class="word-color"> <?php echo(json_decode(file_get_contents("src/data_out/data.json"), true)["nb_candidat_sup_500"]) ?> </span> <span class="legend"> <br/>Candidats + 500 parrainages</span>  </h2>   
                </div>
                <div class="col-3">
                    <h2> <span class="word-color"> <?php echo(json_decode(file_get_contents("src/data_out/data.json"), true)["date_fin"]) ?> </span> <span class="legend"> <br/>Date limite</span>  </h2>
                </div>
            </div>
        </div>

        <div class="container-with-centered-content pb-4">
            <h4>2017</h4>
            <div class="row">
                <div class="col-3">
                    <h2> <span class="word-color"> <?php echo(json_decode(file_get_contents("src/data_out/data.json"), true)["nb_parrainage_valid_2017"]) ?> </span>  / <?php echo(json_decode(file_get_contents("src/data_out/data.json"), true)["nb_parrainage_possible"]) ?> <span class="legend"> <br/>Parrainages exprimés</span></h2>
                </div>
                <div class="col-3">
                    <h2> <span class="word-color"> <?php echo(json_decode(file_get_contents("src/data_out/data.json"), true)["nb_candidat_2017"]) ?> </span> <span class="legend"> <br/>Candidats</span> </h2>
                </div>
                <div class="col-3">
                    <h2> <span class="word-color"> <?php echo(json_decode(file_get_contents("src/data_out/data.json"), true)["nb_candidat_sup_500_2017"]) ?> </span> <span class="legend"> <br/>Candidats + 500 parrainages</span>  </h2>   
                </div>
            </div>
        </div>

        <div class="pb-4">
            <div class="row">
                <div class="col">
                    <h4>Nombre de parrainages (TOP15)</h4>
                    <?php   include("src/data_out/dataviz_nb_DIV.php"); ?>
                </div>
            </div>
        </div>

        <div class="pb-4">
            <div class="row">
                <div class="col">
                    <h4>Type de mandat des parrains</h4>
                    <?php   include("src/data_out/dataviz_pie2_DIV.php"); ?>
                </div>
                <div class="col">
                    <h4>Répartition parrainages homme femme</h4>
                    <?php   include("src/data_out/dataviz_pie_DIV.php"); ?>
                </div>
            </div>
        </div>

        <div class="pb-4">
            <div class="row">
                <div class="col">
                    <h4>Répartition parrainages homme femme par candidats (TOP15)</h4>
                    <?php   include("src/data_out/dataviz_bar_hf_DIV.php"); ?>
                </div>
            </div>
        </div>

        <div class="pb-4">
            <div class="row">
                <div class="col">
                    <h4>Nombre de parrainages validés par le Conseil constitutionnel par candidat (TOP15)</h4>
                    <?php   include("src/data_out/dataviz_line_candidat_DIV.php"); ?>
                </div>
            </div>
        </div>

        <div class="pb-4">
            <div class="row">
                <div class="col">
                    <h4>Nombre de parrainages validés par le Conseil constitutionnel en 2017 et 2022</h4>
                    <?php   include("src/data_out/dataviz_line_DIV.php"); ?>
                </div>
            </div>
        </div>

        <div class="pb-4-map">
            <div class="row">
                <div class="col">
                    <h4>Repartition des parrainages validés par le Conseil constitutionnel par regions</h4>
                    <iframe class="iframe-regions" src="src/data_out/map.html" title="map"> </iframe>
                </div>
            </div>
        </div>

    </section>


    <!-- CONTACT -->
    <div class="footer">
        <div class="footer-txt">
            <!-- Email en character entities : http://www.wbwip.com/wbw/emailencoder.html -->
            <p>Contact : &#097;&#100;&#114;&#105;&#101;&#110;&#046;&#115;&#097;&#105;&#110;&#116;&#064;&#108;&#105;&#118;&#101;&#046;&#099;&#111;&#109; <p>
        </div>
    </div>



<!-- SCRIPT FIG -->
<script src="https://cdn.plot.ly/plotly-latest.min.js"></script> 
<?php include("src/data_out/dataviz_nb_SCRIPT.php"); ?>
<?php include("src/data_out/dataviz_pie2_SCRIPT.php"); ?>
<?php include("src/data_out/dataviz_pie_SCRIPT.php"); ?>
<?php include("src/data_out/dataviz_bar_hf_SCRIPT.php"); ?>
<?php include("src/data_out/dataviz_line_SCRIPT.php"); ?>
<?php include("src/data_out/dataviz_line_candidat_SCRIPT.php"); ?>
</body>
</html>
