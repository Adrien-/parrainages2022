# %%
######################################################################
## BIBLIOTHEQUE GENERIQUE
######################################################################

import os
import pandas as pd
import requests
import datetime as dt
import plotly
import plotly.graph_objects as go
import json
import folium
from folium import GeoJson
import branca



######################################################################
## COLOR
######################################################################
color1 = '#001219'
color2 = '#005f73'
color3 = '#0a9396'
color4 = '#94d2bd'
color5 = '#e9d8a6'
color6 = '#ee9b00'
color7 = '#ca6702'
color8 = '#bb3e03'
color9 = '#ae2012'
color10 = '#9b2226'
color11 = '#FFD700' #gold M
color12 = '#48D1CC' #mediumturquoise Mme
colorb = '#eeebd3'



######################################################################
## Ecriture dans un fichier JSON
##
## Parametres : ROOT_DIR, folder_name, file_name, key_name, value
## Retour : /
##
## Exemple : 
##      ROOT_DIR = os.path.realpath(os.path.join(os.path.dirname(__file__), '..'))   
##      write_in_json(ROOT_DIR,'data_out','data.json','test', 5)
######################################################################
# import json

def write_in_json(ROOT_DIR, folder_name, file_name, key_name, value): 
    with open( (os.path.join(ROOT_DIR, folder_name, file_name)) , 'r') as file:
        json_data = json.load(file)
        json_data[key_name] = value

    with open( (os.path.join(ROOT_DIR, folder_name, file_name)) , 'w') as file:
        json.dump(json_data, file, indent=2)



######################################################################
## Enregistrement des figures dans des fichier php (Global, div, script)
##
## Parametres : ROOT_DIR, fig, folder_name, dataviz_name
## Retour : /
##
## Exemple : 
##      ROOT_DIR = os.path.realpath(os.path.join(os.path.dirname(__file__), '..'))   
##      save_fig_out_div_script(ROOT_DIR, fig, 'data_out', 'dataviz_nb')
######################################################################
def save_fig_out_div_script(ROOT_DIR, fig, folder_name, dataviz_name): 
    f = open( (os.path.join(ROOT_DIR, folder_name, dataviz_name+'_ALL.php')) , "w")
    f.write( plotly.offline.plot(fig, include_plotlyjs=False, output_type='div') )
    f.close()

    # SPLIT DIV AND SCRIPT
    book = open( (os.path.join(ROOT_DIR, folder_name, dataviz_name+'_ALL.php')) ).read()

    div = book.split('<script type="text/javascript">', 1)[0]
    div = div + ' </div>'

    script = book.split('<script type="text/javascript">', 1)[1]
    script = '<script type="text/javascript">' + script
    script = script[:-6]

    f = open( (os.path.join(ROOT_DIR, folder_name, dataviz_name+'_DIV.php')) , "w")
    f.write( div )
    f.close()
    f = open( (os.path.join(ROOT_DIR, folder_name, dataviz_name+'_SCRIPT.php')) , "w")
    f.write( script )
    f.close()



# %%
