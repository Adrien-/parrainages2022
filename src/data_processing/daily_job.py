# %%
####################
## IMPORT 
####################
from package_generique import *



####################
## PATH
####################
ROOT_DIR = os.path.realpath(os.path.join(os.path.dirname(__file__), '..'))




####################
## GET DATA 
####################
r = requests.get('https://presidentielle2022.conseil-constitutionnel.fr/telechargement/parrainagestotal.json')
r.encoding='utf-8-sig'
df = pd.DataFrame.from_dict(pd.json_normalize(r.json()), orient='columns')

df_2017 = pd.read_excel(ROOT_DIR+'/data_in/parrainagestotal2017.xlsx')



####################
## PREPARATION GLOABLE 
####################

# Split DatePublication 2022-02-01T00:00:00 to 2022-02-01
df[['DatePublication', 'sup']] = df['DatePublication'].str.split('T', 1, expand=True)
df.drop('sup', axis=1, inplace=True)



####################
## DATE MAJ  + NB CANDIDAT + NB PARRAINAGES EXPRIMES + NB CANDIDAT SUP 500 PARRAINAGES
####################

date_maj = df.DatePublication.unique()[-1]
write_in_json(ROOT_DIR,'data_out','data.json','date_maj', date_maj)

nb_candidat = len(df.Candidat.unique())
write_in_json(ROOT_DIR,'data_out','data.json','nb_candidat', nb_candidat)

nb_parrainage_exp = df.shape[0]
write_in_json(ROOT_DIR,'data_out','data.json','nb_parrainage_exp', nb_parrainage_exp)

nb_candidat_sup_500 = len(df.Candidat.value_counts().loc[lambda x : x>500])
write_in_json(ROOT_DIR,'data_out','data.json','nb_candidat_sup_500', nb_candidat_sup_500)



####################
## FIG 01 
####################
fig = go.Figure(go.Bar(
            x=df.Candidat.value_counts().tolist()[0:15],
            y=df.Candidat.value_counts().index.tolist()[0:15],
            text=df.Candidat.value_counts().tolist()[0:15],
            orientation='h'))

fig.update_layout(yaxis={'categoryorder':'total ascending'})
fig.add_vline(x=500, line_width=5, line_color=color9)

fig.update_layout(
    autosize=True,
    # width=700,
    # height=500,
    margin=dict(l=40, r=40, t=40, b=40),
    paper_bgcolor='rgba(0,0,0,0)',
    plot_bgcolor='rgba(0,0,0,0)',
    font_family="Arial",
    font_color=colorb,
    title_font_family="Arial",
    title_font_color=colorb,
    legend_title_font_color=colorb,
    font_size=20
)

fig.update_traces(marker_color=colorb, marker_line_color=colorb,
                  marker_line_width=1.5, opacity=.9)

# fig.show()
# fig.write_html("../data_out/dataviz_nb.html")

save_fig_out_div_script(ROOT_DIR, fig, 'data_out', 'dataviz_nb')



####################
## SANKEY DIAGRAM H/F
####################
# https://towardsdatascience.com/sankey-diagram-basics-with-pythons-plotly-7a13d557401a

# FIXME: a fiabiliser (POUR LE MOMENT PAS UTILISE)

color_node = [
color11,color12,color1,color10,color2,
color9,color1,color4,color8,color3,
color7,color2,color7,color6,color3
]

color_link = [
color11,color11,color11,color11,color11,
color11,color11,color11,color11,color11,
color11,color11,color11,color11,color11,
color12,color12,color12,color12,color12,
color12,color12,color12,color12,color12,
color12,color12,color12,color12,color12,
]
label = df.Civilite.value_counts().index.tolist() + df.Candidat.value_counts().index.tolist()
source = [0,0,0,0,0, 0,0,0,0,0, 0,0,0,0,0, 1,1,1,1,1,1, 1,1,1,1,1, 1,1,1,1,1]
target = [2,3,4,5,6,7,8,9,10,11,12,13,14,15,16, 2,3,4,5,6,7,8,9,10,11,12,13,14,15,16]
value = (df[df.Civilite == 'M.'].Candidat.value_counts().tolist()[0:15]) + (df[df.Civilite == 'Mme'].Candidat.value_counts().tolist()[0:15]) 
link = dict(source=source, target=target, value=value, color=color_link)
node = dict(label=label, pad=50, thickness=5, color=color_node)
data = go.Sankey(link=link, node=node)# plot
fig = go.Figure(data)

fig.update_layout(
    autosize=True,
    # width=700,
    # height=500,
    margin=dict(l=40, r=40, t=40, b=40),
    paper_bgcolor='rgba(0,0,0,0)',
    plot_bgcolor='rgba(0,0,0,0)',
    font_family="Arial",
    font_color=colorb,
    font_size=20
)

# fig.show()
# fig.write_html("../data_out/dataviz_sa.html")

# save_fig_out_div_script(ROOT_DIR, fig, 'data_out', 'dataviz_sa')



####################
## BAR DIAGRAM H/F
####################

df_top15 = df[df.Candidat.isin(df.Candidat.value_counts()[0:15].index.tolist())]

fig = go.Figure(data=[
    go.Bar(name='Femme', marker_color=color12, x=df_top15.Candidat.value_counts().sort_index().index.tolist(), y=df_top15[df_top15.Civilite == 'Mme'].Candidat.value_counts().sort_index().tolist()),
    go.Bar(name='Homme', marker_color=color11, x=df_top15.Candidat.value_counts().sort_index().index.tolist(), y=df_top15[df_top15.Civilite == 'M.'].Candidat.value_counts().sort_index().tolist())
])

fig.update_layout(barmode='stack')

fig.update_layout(
    autosize=True,
    # width=700,
    # height=500,
    margin=dict(l=40, r=40, t=40, b=40),
    paper_bgcolor='rgba(0,0,0,0)',
    plot_bgcolor='rgba(0,0,0,0)',
    font_family="Arial",
    font_color=colorb,
    showlegend=True,
)

# fig.show()
# fig.write_html("../data_out/dataviz_bar_hf.html")

save_fig_out_div_script(ROOT_DIR, fig, 'data_out', 'dataviz_bar_hf')



####################
## PIE DIAGRAM  H/F
####################

labels = df.Civilite.value_counts().index.tolist()
values = df.Civilite.value_counts().tolist()

fig = go.Figure(data=[go.Pie(labels=labels, values=values, hole=.3)])

fig.update_layout(
    autosize=True,
    # width=700,
    # height=500,
    margin=dict(l=40, r=40, t=40, b=40),
    paper_bgcolor='rgba(0,0,0,0)',
    plot_bgcolor='rgba(0,0,0,0)',
    font_family="Arial",
    font_color=colorb,
    showlegend=False,
)

colors = [color11, color12]
fig.update_traces(hoverinfo='label+percent+value', textinfo='label+percent', textfont_size=20,
                  marker=dict(colors=colors, line=dict(color='#000000', width=2)))

# fig.show()
# fig.write_html("../data_out/dataviz_pie.html")

save_fig_out_div_script(ROOT_DIR, fig, 'data_out', 'dataviz_pie')



####################
## PIE DIAGRAM  Type de parrains 
####################
# df.Mandat.value_counts().index.tolist()
nb_maire = len(df[df.Mandat.str.contains('Maire',case=False)])
nb_cons_dep = len(df[df.Mandat.str.contains('départemental',case=False)])
nb_cons_reg = len(df[df.Mandat.str.contains('régional',case=False)])
nb_deput = len(df[df.Mandat.str.contains('déput',case=False)])
nb_senat = len(df[df.Mandat.str.contains('sénat',case=False)])
nb_autre = len(df) - nb_maire - nb_cons_dep - nb_cons_reg - nb_deput - nb_senat

labels = ['Maire', 'Conseiller départemental', 'Conseiller régional', 'Député', 'Sénateur', 'Autres']
values = [nb_maire, nb_cons_dep, nb_cons_reg, nb_deput, nb_senat, nb_autre]

fig = go.Figure(data=[go.Pie(labels=labels, values=values, hole=.3)])

fig.update_layout(
    autosize=True,
    # width=700,
    # height=500,
    margin=dict(l=40, r=40, t=40, b=40),
    paper_bgcolor='rgba(0,0,0,0)',
    plot_bgcolor='rgba(0,0,0,0)',
    font_family="Arial",
    font_color=colorb,
    showlegend=False,
)

colors = [color3, color9, color4, color10, color6, color8]
fig.update_traces(hoverinfo='label+percent+value', textinfo='label+percent', textfont_size=20,
                  marker=dict(colors=colors, line=dict(color='#000000', width=2)))

# fig.show()
# fig.write_html("../data_out/dataviz_pie.html")

save_fig_out_div_script(ROOT_DIR, fig, 'data_out', 'dataviz_pie2')



####################
## 2017 vs 2022 
####################

nb_candidat_2017 = len(df_2017['Candidat-e parrainé-e'].unique())
write_in_json(ROOT_DIR,'data_out','data.json','nb_candidat_2017', nb_candidat_2017)

nb_parrainage_valid_2017 = df_2017.shape[0]
write_in_json(ROOT_DIR,'data_out','data.json','nb_parrainage_valid_2017', nb_parrainage_valid_2017)

nb_candidat_sup_500_2017 = len(df_2017['Candidat-e parrainé-e'].value_counts().loc[lambda x : x>500])
write_in_json(ROOT_DIR,'data_out','data.json','nb_candidat_sup_500_2017', nb_candidat_sup_500_2017)



####################
## LINE 
####################

#2022
tot = 0
nb_jour_sum = []
for nb_jour in df.DatePublication.value_counts().sort_index().index:
    tot = tot + len(df[df.DatePublication == nb_jour])
    nb_jour_sum.append(tot) 

#2017
tot_2017 = 0
nb_jour_sum_2017 = []
for nb_jour in df_2017['Date de publication'].value_counts().sort_index().index:
    tot_2017 = tot_2017 + len(df_2017[df_2017['Date de publication'] == nb_jour])
    nb_jour_sum_2017.append(tot_2017) 

# Création d'un X cohérent entre 2017 et 2022 (partie fixe + add auto des new 2022)
x = ['2022-02-01 | 2017-03-01', '2022-02-03 | 2017-03-03', '2022-02-08 | 2017-03-07', '2022-02-10 | 2017-03-10', '2022-02-15 | 2017-03-14','2022-02-17 | 2017-03-18']
for new_date in df.DatePublication.value_counts().sort_index().index[6:].tolist():
    # x.append(new_date)
    # x.append('2022-02-éé | 2017-03-xx')
    x.append(new_date + ' |')

trace_2022 = dict(
    # x=df.DatePublication.value_counts().sort_index().index,
    x = x,
    y=nb_jour_sum,
    mode = 'lines+markers',
    name = '2022',
    line = dict(shape = 'spline', smoothing = 1.3, color = color11, width= 4),
    connectgaps = True
)

trace_2017 = dict(
    # x=df_2017['Date de publication'].value_counts().sort_index().index,
    x = x,
    y=nb_jour_sum_2017,
    mode = 'lines+markers',
    name = '2017',
    line = dict(shape = 'spline', smoothing = 1.3, color = color12, width= 4),
    connectgaps = True
)

data = [trace_2022,trace_2017]

layout =  dict(
    # title = '',
    hovermode='x unified',
    margin=dict(l=40, r=40, t=40, b=40),
    paper_bgcolor='rgba(0,0,0,0)',
    plot_bgcolor='rgba(0,0,0,0)',
    font_family="Arial",
    font_color="#eeebd3",
    legend_title_font_color=color6,
    xaxis=dict(
        # xaxis = dict(title = ''),
        showline=True,
        showgrid=False,
        linecolor='rgb(204, 204, 204)',
    ),
    yaxis=dict(
        dict(title = 'Nombre de parrainages validés'),
        # showgrid=False,
        zeroline=False,
        showline=False,
    ),
    hoverlabel=dict(
        bgcolor="#000000",
        font_size=16
    )
)

fig =  go.Figure(data = data, layout=layout)

fig.add_annotation(
        x='2022-02-17 | 2017-03-18',
        y=14296,
        text=" <b>14 296</b> parrainages validés en 2017",
        showarrow=False,
        yshift=20,
        xshift=-100,
        font=dict(
            # family="Courier New, monospace",
            size=14,
            color='#000000'
            ),
        align="center",
        borderpad=8,
        bgcolor=color12,
        opacity=0.9
        )

# fig.show()
# fig.write_html("../data_out/dataviz_line.html")

save_fig_out_div_script(ROOT_DIR, fig, 'data_out', 'dataviz_line')



####################
## LINE CANDIDAT
####################

# df_top15 = df[df.Candidat.isin(df.Candidat.value_counts()[0:15].index.tolist())]
# df_top15.Candidat.value_counts().sort_index().index.tolist()

layout =  dict(
    # title = '',
    hovermode='x unified',
    margin=dict(l=40, r=40, t=40, b=40),
    paper_bgcolor='rgba(0,0,0,0)',
    plot_bgcolor='rgba(0,0,0,0)',
    font_family="Arial",
    font_color="#eeebd3",
    legend_title_font_color=color6,
    xaxis=dict(
        # xaxis = dict(title = ''),
        showline=True,
        showgrid=False,
        linecolor='rgb(204, 204, 204)',
    ),
    yaxis=dict(
        dict(title = 'Nombre de parrainages validés'),
        # showgrid=False,
        zeroline=False,
        showline=False,
    ),
    hoverlabel=dict(
        bgcolor="#000000",
        font_size=16
    )
)

fig =  go.Figure(layout=layout)

i_color = 0
color = [color1,color2,color3,color4,color5,color6,color7,color8,color9,color10,color11,color12,color1,color2,color3]
for candidat_select in df_top15.Candidat.value_counts().sort_index().index.tolist():
    
    tot = 0
    nb_jour_sum = []
    df_tmp = df[df.Candidat == candidat_select]
    for nb_jour in df_tmp.DatePublication.value_counts().sort_index().index:
        tot = tot + len(df_tmp[df_tmp.DatePublication == nb_jour])
        nb_jour_sum.append(tot) 

    fig.add_traces(go.Scatter(
        x = df.DatePublication.value_counts().sort_index().index,
        y = nb_jour_sum,
        mode = 'lines+markers',
        name = candidat_select,
        line = dict(shape = 'spline', smoothing = 1.3, color = color[i_color], width= 4),
        connectgaps = True
        ))

    i_color = i_color + 1

# fig.show()
# fig.write_html("../data_out/dataviz_line.html")

save_fig_out_div_script(ROOT_DIR, fig, 'data_out', 'dataviz_line_candidat')



####################
## MAP
####################

# https://www.sigtv.fr/L-IGN-edite-la-nouvelle-carte-de-la-France-administrative-et-calcule-les-centres-geographiques-de-chaque-departement_a334.html
# https://www.coordonnees-gps.fr/carte/pays/FR
df_coord = pd.read_excel(ROOT_DIR+'/data_in/lat_long_dep.xlsx')

geo = json.load(open(ROOT_DIR+"/data_in/departements.geojson"))

df_departement = df[["Departement"]]
df_departement = df_departement.value_counts().rename_axis('DEPARTEMENT').reset_index(name='COUNTS')
# upper str
df_departement['DEPARTEMENT'] = df_departement['DEPARTEMENT'].str.upper()
# remove accents
df_departement['DEPARTEMENT'] = df_departement['DEPARTEMENT'].str.normalize('NFKD').str.encode('ascii', errors='ignore').str.decode('utf-8')

df_coord = df_coord[["DEPARTEMENT","LATITUDE_DD","LONGITUDE_DD"]]
df_departement = df_departement.merge(df_coord, on='DEPARTEMENT', how='left')

# DROP NAN  + sum des dom tom  14 LIGNES
# df_departement.isna().sum()
sum_nan = df_departement[df_departement.LATITUDE_DD.isna()].COUNTS.sum()
df_departement = df_departement.dropna()
df_departement = df_departement.reset_index(drop=True)

# map + fond de carte
france = folium.Map(location=[46.232192999999995, 2.209666999999996], tiles='cartodbdark_matter', zoom_start=6,min_zoom=6,max_zoom=10) # OpenStreetMap, cartodbdark_matter, Stamen Toner

GeoJson(geo,
       style_function=lambda feature: {    
        'fillColor': '#27ae60',           
        'color': '#000000', 
        'weight': 1,
    }).add_to(france)

# popup style
def popup_html(i):
    name_dep=df_departement["DEPARTEMENT"].iloc[i]
    count=str(df_departement["COUNTS"].iloc[i])

    left_col_color = "#19a7bd"
    right_col_color = "#f2f0d3"
    classement = str(i+1) + " / 96 dep"
    
    html = """<!DOCTYPE html>
    <html>
    <head>
    <h4 style="margin-bottom:10"; width="200px">{}</h4>""".format(name_dep) + """
    </head>
        <table style="height: 24px; width: 250px;">
    <tbody>
    <tr>
    <td style="background-color: """+ left_col_color +"""; padding-left:10px;"><span style="color: #ffffff;">Classement</span></td>
    <td style="width: 90px; padding-left:10px; background-color: """+ right_col_color +""";">{}</td>""".format(classement) + """
    </tr>
    <tr>
    <td style="background-color: """+ left_col_color +"""; padding-left:10px;"><span style="color: #ffffff;">Nombre de parrainages</span></td>
    <td style="width: 90px; padding-left:10px; background-color: """+ right_col_color +""";">{}</td>""".format(count) + """
    </tr>
    </tbody>
    </table>
    </html>
    """
    return html

for i in range(0,len(df_departement)):
    html = popup_html(i)
    iframe = branca.element.IFrame(html=html,width=510,height=280)
    popup = folium.Popup(folium.Html(html, script=True), max_width=500)
    folium.CircleMarker(
                        location=[df_departement["LATITUDE_DD"][i],df_departement["LONGITUDE_DD"][i]],
                        # popup = "Département : " + df_departement["DEPARTEMENT"][i] + " Nombre de parrainages : " + str(df_departement["COUNTS"][i]),    
                        popup = popup,    
                        radius = df_departement["COUNTS"][i] /10,
                        color='white',
                        fill=True,
                        ).add_to(france)

france.save(ROOT_DIR+"/data_out/map.html")



# %%
