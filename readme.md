# Dataviz des parrainages des candidats à l'élection présidentielle française de 2022

[Visite le site web!](http://parrainages2022.adriensaint.fr/)

<a href="http://parrainages2022.adriensaint.fr/"> <img src="http://img.adriensaint.fr/img-website-parrainages2022.png" alt="img-website-parrainages" title="Visite le site web!" width="500"/> </a>

## Dernier déploiement [![pipeline status](https://gitlab.com/Adrien-/parrainages2022/badges/master/pipeline.svg)](https://gitlab.com/Adrien-/parrainages2022/-/commits/master) 

---

Contact : [AS](https://adriensaint.fr/)
